unit mFunctions;

{$mode objfpc}{$H+}

interface

uses
   math, Classes;

type
   nlFunc =  function(x : real) : real;

   MathFunction = class
   protected
      _caption : string;
      _func : nlFunc;

   public
      constructor create(caption : string; func : nlFunc);

      function getCaption() : string;
      function getSol(x : real) : real;
   end;


   MathFuncDer = class(MathFunction)
   private
      _deriv : nlFunc;

   public
      constructor create(caption : string; func, deriv : nlFunc);

      function getDeriv(x : real) : real;
   end;

   MathFuncFixP = class(MathFunction)
   private
      _fixPderiv : nlFunc;
      _fixPfunc : nlFunc;
   public
      constructor create(caption : string; func, fixP, deriv : nlFunc);

      function getFixPfunc(x : real) : real;
      function getFixPderiv(x : real) : real;
   end;



function func1(x : real) : real;
function func2(x : real) : real;
function func3(x : real) : real;
function func3Der(x : real) : real;
function func4(x : real) : real;
function func5(x : real) : real;
function func6(x : real) : real;
function func7(x : real) : real;
function func5Der(x : real) : real;
function func6Der(x : real) : real;
function func7Der(x : real) : real;
function func8(x : real) : real;
function func8FPDer(x : real) : real;
function func8FP(x : real) : real;

function func4Der(x : real) : real;

implementation

constructor MathFunction.create(caption : string; func : nlFunc);
begin
   _caption := caption;
   _func := func;
end;

function MathFunction.getCaption() : string;
begin
   Result := _caption;
end;

function MathFunction.getSol(x : real) : real;
begin
   Result := _func(x);
end;

constructor MathFuncDer.create(caption : string; func, deriv : nlFunc);
begin
   inherited create(caption, func);
   _deriv := deriv;
end;

function MathFuncDer.getDeriv(x : real) : real;
begin
   Result := _deriv(x);
end;

constructor MathFuncFixP.create(caption : string; func, fixP, deriv : nlFunc);
begin
   inherited create(caption, func);
   _fixPfunc := fixP;
   _fixPderiv := deriv;
end;

function MathFuncFixP.getFixPfunc(x : real) : real;
begin
   Result := _fixPfunc(x);
end;

function MathFuncFixP.getFixPderiv(x : real) : real;
begin
   Result := _fixPderiv(x);
end;


// --------------------------------------

function func1(x : real) : real;
begin
   Result := x * x - ln(x) * exp(x);
end;

function func2(x : real) : real;
begin
   Result := x * x - exp(x);
end;

function func3(x : real) : real;
begin
   Result := ln(x) * sin(x);
end;




//newton...
function func5(x : real) : real;
begin
   Result := exp(-x) - ln(x);
end;

function func6(x : real) : real;
begin
   Result := exp(-x) - ln(-x) - 2 * x * x;
end;

function func7(x : real) : real;
begin
   Result := sin(exp(x) - x) / x;
end;

function func3Der(x : real) : real;
begin
   Result := sin(x)/ x + ln(x) * cos(x);
end;

function func5Der(x : real) : real;
begin
   Result := -exp(-x) - 1/x;
end;

function func6Der(x : real) : real;
begin
   Result := -exp(-x) + 1/x - 4*x;
end;

function func7Der(x : real) : real;
begin
   Result := (cos(exp(x) - x) * (exp(x) - 1) - sin(exp(x) - x) )/ (x*x);
end;

//fixed point ...
function func8(x : real) : real;
begin
   Result := ln(x);
end;

function func8FP(x : real) : real;
begin
   Result := exp(x);
end;

function func8FPDer(x : real) : real;
begin
   Result := exp(x);
end;


//Newton y secante
function func4(x : real) : real;
begin
   Result := x*x*x - 3*x + 2;
end;

function func4Der(x : real) : real;
begin
   Result := 3*x*x - 3; //(2 - x)/ power((x - 1),2);
end;
end.

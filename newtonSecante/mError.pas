unit mError;

{$mode objfpc}{$H+}

interface

uses Math;

function absError(r : real; c : real ) : real;
function relError(r : real; c : real ) : real;
function perError(r : real; c : real ) : real;

implementation

function absError(r : real; c : real ) : real;
begin
   Result := abs(r - c);
end;

function relError(r : real; c : real ) : real;
begin
   Result := abs((r - c) / r);
end;

function perError(r : real; c : real ) : real;
begin
   Result := abs((r - c) / r) * 100;
end;

end.

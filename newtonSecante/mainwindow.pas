unit mainWindow;

{$mode objfpc}{$H+}

interface

uses
   Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, Grids,
   StdCtrls, mController, mFunctions;

type

   { TForm1 }

   TForm1 = class(TForm)
      BtnGen: TButton;
      CBFunction: TComboBox;
      CBMethod: TComboBox;
      EditLowLimit: TEdit;
      EditHighLimit: TEdit;
      EditMinErr: TEdit;
      EditPnt: TEdit;
      Label1: TLabel;
      Label2: TLabel;
      Label3: TLabel;
      Label4: TLabel;
      Label5: TLabel;
      Label6: TLabel;
      GridTable: TStringGrid;
      procedure BtnGenClick(Sender: TObject);
      procedure FormCreate(Sender: TObject);
   private
      ppControl : Controller;
      function showError(perror : ErrorType; a, b : real) : boolean;
      procedure CleanStringGrid;
   public
      { public declarations }
   end;

var
   Form1: TForm1;

implementation

{$R *.lfm}

{ TForm1 }

procedure TForm1.FormCreate(Sender: TObject);
var
   i : integer;
begin
   ppControl := Controller.create();
   CBFunction.Clear;
   for i := 0 to ppControl.funcList.Count - 1 do
      CBFunction.Items.Add((ppControl.funcList[i] as MathFunction).getCaption());
   CBFunction.ItemIndex := 0;

   CBMethod.Clear;
   CBMethod.Items.Add('Newton');
   CBMethod.Items.Add('Secante');
   CBMethod.Items.Add('Punto Fijo');
   CbMethod.ItemIndex := 0;
end;

function TForm1.showError(perror: ErrorType; a, b : real): boolean;
begin
   case perror of
      NonCFirstL :
         begin
            ShowMessage('ERROR: ' + sLineBreak
                        + 'La función no es continua en el primer límite.' );
            exit(true);
         end;
      NonCSecondL :
         begin
            ShowMessage('ERROR: ' + sLineBreak
                        + 'La función no es continua en el segundo límite.');
            exit(true);
         end;
      AnswFirstL :
         begin
            ShowMessage('La respuesta es: ' + FloatToStr(a) );
            exit(true);
         end;
      AnswSecondL :
         begin
            ShowMessage('La respuesta es: ' + FloatToStr(b) );
            exit(true);
         end;
      NoBolzPosPos:
         begin
            ShowMessage('El teorema de Bolzano no se cumple para los límites ingresados.'
                        + sLineBreak + 'Las imágenes de los dos límites son positivas.');
            exit(true);
         end;
      NoBolzNegNeg :
         begin
            ShowMessage('El teorema de Bolzano no se cumple para los límites ingresados.'
                        + sLineBreak + 'Las imágenes de los dos límites son negativas.');
            exit(true);
         end;
      NonCont :
         begin
            ShowMessage('ERROR: ' + sLineBreak
                        + 'La función no es continua en el punto ingresado.');
            exit(true);
         end;

      NoSolution :
         begin
            ShowMessage('No hay solución o la función no es continua.'
                       + sLineBreak + 'En newton/secante: si xn+1 es inf: la pendiente se hizo 0.');
            exit(true);
         end;
      NonDeriv :
         begin
            ShowMessage('No se ha establecido la derivada de la funcion seleccionada');
            exit(true);
         end;
      NonFixP :
         begin
            ShowMessage('No se puede aplicar punto flotante a la función seleccionada:'
                        + sLineBreak + 'No se a implementado g(x) o su derivada.');
            exit(true);
         end;
      NoConvFP :
         begin
            ShowMessage('Es muy probable que la función seleccionada no converga en el punto dado.');
            exit(true);
         end;

      Nothing:
         exit(false);
   end;
end;

procedure TForm1.BtnGenClick(Sender: TObject);
var
   minError : real;
   a, b, pnt: real;
   usedMethod : MethodType;
   funcIndex : Integer;
   tableRow : Integer;
   thisError : ErrorType;
const
   realFormat = '0.######';
begin
   if (EditPnt.GetTextLen = 0) and
      (EditLowLimit.GetTextLen = 0) and (EditHighLimit.GetTextLen = 0) then
   begin
      ShowMessage('No ha ingresado límites ni un punto a evaluar.');
      exit;
   end;

   if (EditHighLimit.GetTextLen <> 0) or (EditLowLimit.GetTextLen <> 0) then
   begin
      if (EditPnt.GetTextLen <> 0) then
      begin
         ShowMessage('No puede ingresar Límites y un punto a la vez.');
         exit;
      end;
      if (EditHighLimit.GetTextLen = 0) or (EditLowLimit.GetTextLen = 0) then
      begin
         ShowMessage('Le falta ingresar un límite.');
         exit;
      end;
   end;

   if (EditPnt.GetTextLen <> 0) then
   begin
      if (EditHighLimit.GetTextLen <> 0) or (EditLowLimit.GetTextLen <> 0) then
      begin
         ShowMessage('No puede ingresar Límites y un punto a la vez.');
         exit;
      end;
   end;

   if (EditMinErr.GetTextLen = 0) then
   begin
      ShowMessage('No ha ingresado el error mínimo.');
      exit;
   end;

   minError := StrToFloat(EditMinErr.Text);

   case CBMethod.ItemIndex of
   0 : usedMethod := NewtonMet;
   1 : usedMethod := SecantMet;
   2 : usedMethod := FixPointMet;
   end;

   funcIndex := CBFunction.ItemIndex;

   if (EditHighLimit.GetTextLen <> 0) then
   begin
      a := StrToFloat(EditLowLimit.Text);
      b := StrToFloat(EditHighLimit.Text);
      thisError := ppControl.setVals(usedMethod, funcIndex, minError, a, b);
   end
   else
   begin
      pnt := StrToFloat(EditPnt.Text);
      thisError := ppControl.setVals(usedMethod, funcIndex, minError, pnt);
   end;

   if (self.showError(thisError,a ,b)) then
      exit;

   self.CleanStringGrid;
   tableRow := 1;
   while (not ppControl.isDone()) do
   begin
      thisError := ppControl.process();
      if (self.showError(thisError,a ,b)) then
         exit;

      GridTable.InsertRowWithValues(tableRow,[]);
      GridTable.Cells[0,tableRow] := IntToStr(tableRow - 1);
      GridTable.Cells[1,tableRow] := FormatFloat(realFormat, ppControl.point);
      GridTable.Cells[2,tableRow] := FormatFloat(realFormat, ppControl.nextPoint);
      GridTable.Cells[3,tableRow] := FormatFloat(realFormat, ppControl.currentError);
      tableRow := tableRow + 1;
   end;

   thisError := ppControl.hasSolution();
   if (self.showError(thisError,a ,b)) then
      exit;
end;

procedure TForm1.CleanStringGrid;
var
   I: Integer;
begin
   for I := 0 to GridTable.ColCount - 1 do
      GridTable.Cols[I].Clear;
   GridTable.RowCount := 1;
end;


end.

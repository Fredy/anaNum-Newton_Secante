unit mController;

{$mode objfpc}{$H+}

interface

uses
   math, Classes, Contnrs,
   mFunctions, mMethods, mError;

type
   ErrorType = (NonCont,  NoSolution,
                NonDeriv, NonFixP,
                NonCFirstL, NonCSecondL,
                AnswFirstL, AnswSecondL,
                NoBolzPosPos, NoBolzNegNeg,
                NoConvFP,
                Nothing);

   MethodType = (BisectionMet, FalsePosMet, NewtonMet, SecantMet, FixPointMet);

   Controller = class
   private
      function isNanorInf(const d: real) :boolean ;

   public
      funcList : TObjectList;
      funcIndex : integer;
      methodFunc : MethodType;
      limitA : real;
      limitB : real;
      point : real;
      minError : real;
      currentError : real;
      nextPoint : real;


      constructor create();
      destructor destroy(); override;

      function setVals(met : MethodType; func : integer; minErr: real;  limA, limB : real) : ErrorType; overload;
      function setVals(met : MethodType; func : integer; minErr : real; pnt : real): ErrorType; overload;

      function process() : ErrorType;
      function isDone() : boolean;
      function hasSolution() : ErrorType;
      procedure clear();

   end;

implementation

constructor Controller.create();
begin
   funcList := TObjectList.create();

   funcList.add(MathFunction.create('x² - ln(x)eˣ', @func1));
   funcList.add(MathFunction.create('x² - eˣ', @func2));
   funcList.add(MathFuncDer.create('ln(x)sin(x)', @func3, @func3Der));
   funcList.add(MathFuncDer.create('e⁻ˣ - ln(x)', @func5, @func5Der));
   funcList.add(MathFuncDer.create('e⁻ˣ - ln(-x) - 2x²', @func6, @func6Der));
   funcList.add(MathFuncDer.create('sin(eˣ - x) / x', @func7, @func7Der));
   funcList.add(MathFuncFixP.create('Para punto fijo', @func8, @func8FP, @func8FPDer));
   funcList.add(MathFuncDer.create('Para newton y secante', @func4, @func4Der));
end;

destructor Controller.destroy();
begin
   funcList.destroy();
end;

function Controller.isNanorInf(const d: real): boolean;
begin
    Result := (isNan(d) or isInfinite(d));
end;

function Controller.setVals(met : MethodType; func : integer; minErr: real;  limA, limB : real) : ErrorType;
var
   sgnA, sgnB : Integer;
   test1, test2 : real;
   i : integer;
begin
   test1 := (funcList[func] as MathFunction).getSol(limA);
   test2 := (funcList[func] as MathFunction).getSol(limB);

   if (self.isNanorInf(test1)) then
      exit(NonCFirstL);

   if (self.isNanorInf(test2)) then
      exit(NonCSecondL);

   sgnA := sign(test1);
   sgnB := sign(test2);
   //TODO : si no funciona reemplazar por esto:
  // sgnA := Sign((funcList[func] as MathFunction).getSol(limA));
  // sgnA := Sign((funcList[func] as MathFunction).getSol(limB));


   if (sgnA = 0) then
      exit(AnswFirstL);

   if (sgnB = 0) then
      exit(AnswSecondL);

   if (sgnA = sgnB) then
   begin
      if (sgnA > 0) then
         exit(NoBolzPosPos)
      else
         exit(NoBolzNegNeg);
   end;

   methodFunc := met;
   funcIndex := func;
   minError := minErr;
   currentError := Infinity;

   // False position iterations to find the first point:
   nextPoint := fullFalsePosition(limA, limB, 3, (funcList[funcIndex] as MathFunction));

   case methodFunc of
      NewtonMet :
         begin
            if not (funcList[funcIndex] is MathFuncDer) then
               exit(NonDeriv);
         end;
      FixPointMet :
         begin
            if not (funcList[funcIndex] is MathFuncFixP) then
               exit(NonFixP);
            test1 := (funcList[func] as MathFuncFixP).getFixPderiv(nextPoint);
            test1 := abs(test1);
            if not ((test1 >= 0) and (test1 <= 1)) then
               exit(NoConvFP);
         end;
   end;


   Result := Nothing;
end;

function Controller.setVals(met : MethodType; func : integer; minErr : real; pnt : real) : ErrorType;
var
   test1 : real;
begin
   test1 := (funcList[func] as MathFunction).getSol(pnt);

   if (self.isNanorInf(test1)) then
      exit(NonCont);

   methodFunc := met;
   funcIndex := func;
   minError := minErr;
   currentError := Infinity;
   nextPoint := pnt;

   case methodFunc of
      NewtonMet :
         begin
            if not (funcList[funcIndex] is MathFuncDer) then
               exit(NonDeriv);
         end;
      FixPointMet :
         begin
            if not (funcList[funcIndex] is MathFuncFixP) then
               exit(NonFixP);
            test1 := (funcList[func] as MathFuncFixP).getFixPderiv(nextPoint);
            test1 := abs(test1);
            if not ((test1 >= 0) and (test1 <= 1)) then
               exit(NoConvFP);
         end;
   end;

   Result := Nothing;
end;

procedure Controller.clear();
begin
   funcIndex := 0;
   point := 0;
   minError := 0;
   currentError := 0;
   nextPoint := 0;

end;

function Controller.process() : ErrorType;
var
   hval : real;
begin
   point := nextPoint;
   case methodFunc of
      // TODO: newton y secante: implementar que bote error cuando la pendiente es 0.
      NewtonMet :
         begin
            nextPoint := newton(point, (funcList[funcIndex] as MathFuncDer));
         end;
      SecantMet :
         begin
            // NOTA: arreglar esto: el hval nunca cambia.
            if (isInfinite(currentError)) then
                  hval := minError   /10
            else
               hval := minError / 10;
            nextPoint := secant(point, hval , (funcList[funcIndex] as MathFunction));
         end;
      FixPointMet :
         begin
            nextPoint := fixPoint(point, (funcList[funcIndex] as MathFuncFixP));
         end;
   end;

   currentError := absError(nextPoint, point);

   if (self.isNanorInf(point)) then
      exit(NoSolution);

   Result := Nothing;
end;

function Controller.isDone() : boolean;
begin
   Result := currentError < minError;
end;

function Controller.hasSolution() : ErrorType;
var
   hval, test1: real;
begin
   while (currentError >= 0.00000000000000000000000001) do
   begin
      point := nextPoint;
      case methodFunc of
         NewtonMet :
            begin
               nextPoint := newton(point, (funcList[funcIndex] as MathFuncDer));
               // El problema de m == 0 ya está implicitamente, si se hace una división entre 0,
               // nextPoint se vuelve inf y se verifica al final. :)
            end;
         SecantMet :
            begin
               if (isInfinite(currentError)) then
                  hval := 0.01
               else
                  hval := currentError / 10;
               nextPoint := secant(point, hval , (funcList[funcIndex] as MathFunction));
            end;
         FixPointMet :
         begin
            nextPoint := fixPoint(point, (funcList[funcIndex] as MathFuncFixP));
         end;
      end;

      currentError := absError(nextPoint, point);


      if (self.isNanorInf(point)) then
         exit(NoSolution);

      test1 := (funcList[funcIndex] as MathFunction).getSol(point);
      if (self.isNanorInf(test1)) then
         exit(NoSolution);

   end;
   Result := Nothing;
end;

end.

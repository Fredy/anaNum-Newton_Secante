program Test123;

{$mode objfpc}{$H+}

uses
   mFunctions, Contnrs, math;

function func1(x : real) : real;
begin
   func1 := x * x - ln(x) * exp(x);
end;

function func2(x : real) : real;
begin
   func2 := x * x - exp(x);
end;

procedure func2(x : integer);
begin
   x := x + 1;
end;

function testfullFalsePosition(a,b: real; iterations : integer; func : nlFunc) : real;
var
   fa, fb, fx: real;
   xn : real;
   i : integer;
begin
   // TODO : bolzano, continuity.

   for i := 1 to iterations do
   begin
      fa := func(a);
      fb := func(b);
      xn := a - fa * (b - a) / (fb - fa);
      fx := func(xn);

      if (sign(fa) = sign(fx)) then
         a := xn
      else
         b := xn;
   end;
   Result := xn;
end;

var
   a1 : MathFunction;
   a2 : MathFuncDer;
   algo : string;
   rr : real;
   asd : real;
   lista : TObjectList;
   tmpFunc : nlFunc;
begin
   a1 := MathFunction.create('una funcion', @func1);
   a2 := MathFuncDer.create('pepito2', @func1, @func2);
   writeln(a2.getCaption());
   writeln(a1.getCaption());
   rr := 1E-2;
   writeln(rr:0 :4);

   lista := TObjectList.create();
   lista.add(MathFunction.create('abcde', @func1));
   lista.add(MathFuncDer.create('12345', @func1, @func2));
   writeln((lista[0] as MathFunction).getCaption());
   writeln((lista[1] as MathFuncDer).getCaption());
   if (lista[0] is MathFuncDer) then
         writeln('Es un objeto de ssd');

   writeln(testfullFalsePosition(1, 3.5, 46, @func1));
   asd := Infinity;
   writeln(asd);
   if (asd > rr) then
         writeln('infinito es mayor a todo');
end.

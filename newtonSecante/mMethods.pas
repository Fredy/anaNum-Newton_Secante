unit mMethods;

{$mode objfpc}{$H+}

interface

uses
   math, mFunctions;

type
   nlFunc = function(x : real) : real;
   methodBF = function(a, b: real; func : MathFunction): real;
   methodNS = function(a : real; func : MathFunction) : real;

function bisectionFunc(a, b: real; func : MathFunction): real;
function falsePositionFunc(a, b: real; func : MathFunction): real;
function newton(xn : real; func : MathFuncDer) : real;
function secant(xn, h : real; func : MathFunction) : real;
function fixPoint(xn : real; func : MathFuncFixP) : real;

function fullFalsePosition(a,b: real; iterations : integer; func : MathFunction) : real;

implementation

function bisectionFunc(a, b: real; func : MathFunction): real;
begin
   Result := 0.5 * (a + b);
end;

function falsePositionFunc(a, b: real; func : MathFunction): real;
var
   fa, fb : real;
begin
   fa := func.getSol(a);
   fb := func.getSol(b);
   Result := a - fa * (b - a) / (fb - fa);
end;

function newton(xn : real; func : MathFuncDer) : real;
var
   fx, fdx : real;
begin
   fx := func.getSol(xn);
   fdx := func.getDeriv(xn);
   Result := xn - (fx/fdx);
end;

function secant(xn, h : real; func : MathFunction) : real;
var
   fx, fxph, fxmh : real;
begin
   fx := func.getSol(xn);
   fxph := func.getSol(xn + h);
   fxmh := func.getSol(xn - h);
   Result := xn - ((fx * 2 * h) / (fxph - fxmh));
end;

function fixPoint(xn : real; func : MathFuncFixP) : real;
begin
   Result := func.getFixPfunc(xn);
end;


function fullFalsePosition(a,b: real; iterations : integer; func : MathFunction) : real;
var
   fa, fb, fx: real;
   xn : real;
   i : integer;
begin
   // TODO : bolzano, continuity.

   for i := 1 to iterations do
   begin
      fa := func.getSol(a);
      fb := func.getSol(b);
      xn := a - fa * (b - a) / (fb - fa);
      fx := func.getSol(xn);

      if (sign(fa) = sign(fx)) then
         a := xn
      else
         b := xn;
   end;
   Result := xn;
end;

end.
